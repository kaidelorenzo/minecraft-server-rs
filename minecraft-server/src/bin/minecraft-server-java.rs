fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = std::env::args().collect();

    let xdg_dirs = xdg::BaseDirectories::with_prefix(
        minecraft_server::CONSTANTS.java_server_xdg_subdirectory,
    )?;

    let jvm_settings_filepath_environment_variable_value =
        std::env::var(minecraft_server::CONSTANTS.jvm_settings_filepath_environment_variable);

    let current_dir_environment_variable_value =
        std::env::var(minecraft_server::CONSTANTS.current_dir_environment_variable);

    let java_home_environment_variable_value = std::env::var("JAVA_HOME")
        .map(|val| -> Option<String> { Some(val) })
        .or_else(|err| -> Result<Option<String>, std::env::VarError> {
            match err {
                std::env::VarError::NotPresent => Ok(None),
                std::env::VarError::NotUnicode(_) => Err(err),
            }
        })?;

    let config = minecraft_server::JavaConfig::build(
        &args,
        &xdg_dirs,
        &jvm_settings_filepath_environment_variable_value,
        &current_dir_environment_variable_value,
        &java_home_environment_variable_value,
    )?;
    minecraft_server::run_java_server(config)
}
