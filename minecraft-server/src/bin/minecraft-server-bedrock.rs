fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = std::env::args().collect();

    let xdg_dirs = xdg::BaseDirectories::with_prefix(
        minecraft_server::CONSTANTS.bedrock_server_xdg_subdirectory,
    )?;

    let current_dir_environment_variable_value =
        std::env::var(minecraft_server::CONSTANTS.current_dir_environment_variable);

    let config = minecraft_server::BedrockConfig::build(
        &args,
        &xdg_dirs,
        &current_dir_environment_variable_value,
    )?;
    minecraft_server::run_bedrock_server(config)
}
